SUJET IP

Par Maxime PIZZOLITTO et Seellen T Padayachi

Le programme est composé de 5 fonctions qui lui permettent de de demander à l'utilisateur une adresse IP, et ainsi lui renvoyer la classe de l’IP, sa visibilité et son adresse réseau. Pour l'utiliser il faut utiliser "make main"


Verifier_format prend en paramètre le pointeur contenant l’IP et grâce à l'utilisation des regex, renvoie 1 si les deux expressions correspondent et 0 dans le cas contraire, le traitement de ces valeurs est ensuite effectué dans le main.

extraire_valeur prend en paramètre un Int qui correspond au bit souhaité, le pointeur de l’IP et le nombre de caractère de l’IP (nous avons rencontré beaucoup de difficultés sur cette fonction). Elle convertit d'abord le pointeur sous forme de tableau, pour pouvoir accéder aux caractères par des indices. Ensuite je cherche la taille du nombre de chaque octets par exemple 192.168.1.56/24 va se transformer en un tableau size qui contient 3,3,1,2,2. J'ai fait ça pour pouvoir lire plus facilement la chaine grâce à une boucle for. Ensuite la fonction renvoie un Int, qui est le bit souhaité. Avec l’IP du dessus si je demande le bit 0 elle me renvoie 192. De plus elle renvoi -1 si x > 256 et <0


Verifier_classe prend en entré un Int, qui est le bit le plus à gauche, et y applique un masque pour déterminer la classe. Elle renvoie un char qui correspond à la classe

verifier_visibilite prend en entré l'adresse IP (convertie en Integer) sous forme de pointeur, ainsi que la classe sous forme de char. Avec des conditions elle détermine la visibilité de l’IP et renvoie ‘x’ si la fonction est privée, ‘z‘ si elle est publique et  ‘e’ si il y a une erreur. 


obtenir_reseau_hote prend en entré l'adresse IP (convertie en Integer) sous forme de pointeur, ainsi que le masque, qui correspond à IP ip_d[4] dans le main. Cette fonction calcule le nombre de fois que 8 est dans le masque pour pouvoir calculer plus facilement l'adresse du réseau, elle effectue aussi un modulo pour avoir l'excédent de bit (avec 18, l'excédent de bit est 2 par exemple). Elle convertie l'excédent de bit en nombre et applique un masque sur le bit cible. Elle affiche directement le résultat.